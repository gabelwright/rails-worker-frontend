import * as API from './../utils/api/user'
import {getPosts} from './post'
import {getComments} from './comment'
export const NEW_USER = 'NEW_USER'
export const LOGGED_IN = 'LOGGED_IN'
export const LOGGED_OUT = 'LOGGED_OUT'
export const GET_PROFILE = 'GET_PROFILE'

export function newUser({first, last, username, email, password}) {
  return {
    type: NEW_USER,
    first,
    last,
    username,
    email,
    password
  }
}

export function newUserAPI({first, last, username, email, password}) {
  return function(dispatch){
    return API.newUser(first, last, username, email, password)
    .then((api_data) => {
      if(!api_data.error){
        dispatch(newUser({first, last, username, email, password}))
      }
      return api_data
    })
  }
}

export function getProfileAPI(){
  return function(dispatch){
    return API.getProfile()
    .then(api_data => {
      if(!api_data.user){
        dispatch(loggedOutUser())
      }
      else if(!api_data.error){
        dispatch(getPosts({data: api_data.data.posts}))
        dispatch(getComments({data: api_data.data.comments}))
      }
      return api_data
    })
  }
}

export function loggedInAPI({username, password, remember}) {
  return function(dispatch){
    return API.login(username, password, remember)
    .then((api_data) => {
      if(api_data.user){
        dispatch(loggedIn(api_data.user))
      }
      return api_data
    })
  }
}

export function checkForLoggedIn(){
  return function(dispatch){
    return API.getLoggedInUser()
    .then((api_data) => {
      if(api_data.user){
        dispatch(loggedIn(api_data.user))
      }
      return api_data
    })
  }
}

export function loggedIn({first, last, username, email, id, confirmed, token}){
  return {
    type: LOGGED_IN,
    first,
    last,
    username,
    email,
    id,
    confirmed,
    token
  }
}

export function updatePasswordAPI({oldPassword, newPassword}){
  return function(dispatch){
    return API.updatePassword(oldPassword, newPassword)
    .then(api_data => api_data)
  }
}

export function verifyTokenAPI({token}){
  return function(dispatch){
    return API.verifyToken(token)
    .then(api_data => {
      if(!api_data.user){
        dispatch(loggedOutUser())
      }
      else if(!api_data.error){
        let {first, last, username, email, id, confirmed} = api_data.user
        dispatch(loggedIn({first, last, username, email, id, confirmed}))
      }
      return api_data
    })
  }
}

export function loggedOutUserAPI(){
  return function(dispatch){
    return API.logout()
    .then((api_data) => {
      if(!api_data.error){
        dispatch(loggedOutUser())
      }
      return api_data
    })
  }
}

export function loggedOutUser(){
  return {
    type: LOGGED_OUT
  }
}
