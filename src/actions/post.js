import * as API from './../utils/api/post'
export const GET_POSTS = 'GET_POSTS'
export const SEND_POST = 'SEND_POST'
export const LOGGED_OUT = 'LOGGED_OUT'
export const DELETE_POST = 'DELETE_POST'
export const GET_POSTS_FROM = 'GET_POSTS_FROM'

export function getPosts({data}){
  return{
    type: GET_POSTS,
    data
  }
}

export function getPostsAPI({topicLocation, limit}){
  return function(dispatch){
    return API.getPosts(topicLocation, limit)
    .then(api_data => {
      if(!api_data.user){
        dispatch(loggedOutPost())
      }
      else if(!api_data.error){
        dispatch(getPosts({data: api_data.data}))
      }
      return api_data
    })
  }
}

function getPostsFrom({data}){
  return{
    type: GET_POSTS_FROM,
    data
  }
}

export function getPostsFromAPI({timestamp, topicLocation}){
  return function(dispatch){
    return API.getPostsFrom(timestamp, topicLocation)
    .then(api_data => {
      if(!api_data.user){
        dispatch(loggedOutPost())
      }
      else if(!api_data.error){
        dispatch(getPostsFrom({data: api_data.data}))
      }
      return api_data
    })
  }
}

export function getSinglePostAPI({postid, location}){
  return function(dispatch){
    return API.getSinglePost(postid, location)
    .then(api_data => {
      return api_data
    })
  }
}

export function editPostAPI({postid, body}){
  return function(dispatch){
    return API.editPost(postid, body)
    .then(api_data => {
      if(!api_data.user){
        dispatch(loggedOutPost())
      }
      return api_data
    })
  }
}

function sendPost({post, topicLocation}){
  return{
    type: SEND_POST,
    post,
    topicLocation
  }
}

export function sendPostAPI({title, body, topicId, topicLocation, userId, anon}){
  return function(dispatch){
    return API.sendPost(title, body, topicId, userId, anon)
    .then(api_data => {
      if(!api_data.user){
        dispatch(loggedOutPost())
      }
      else if(!api_data.error){
        dispatch(sendPost({post: api_data.data, topicLocation}))
      }
      return api_data
    })
  }
}

function deletePost({id}){
  console.log('action')
  return{
    type: DELETE_POST,
    id
  }
}

export function deletePostAPI({id}){
  return function(dispatch){
    return API.deletePost(id)
    .then(api_data => {
      if(!api_data.user){
        dispatch(loggedOutPost())
      }
      else if(!api_data.error){
        dispatch(deletePost({id}))
      }
      return api_data
    })
  }
}

export function loggedOutPost(){
  return{
    type: LOGGED_OUT
  }
}
