import * as API from './../utils/api/post'
export const GET_TOPICS = 'GET_TOPICS'

export function getTopics({data}){
  return{
    type: GET_TOPICS,
    data
  }
}

export function getTopicsAPI(){
  return function(dispatch){
    return API.getTopics()
    .then(api_data => {
      if(!api_data.error){
        dispatch(getTopics({data: api_data.data}))
      }
      return api_data
    })
  }
}
