import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import { Container } from 'semantic-ui-react'

import {getPostsAPI} from './../actions/post'
import {loggedOutUser} from './../actions/user'

import PostCardGrid from './PostCardGrid'
import AuthOnly from './AuthOnly'
import PageNotFound from './PageNotFound'

class Topic extends Component{
  state = {

  }

  componentDidMount(){
    let diffTopic = false
    let posts = this.props.posts
    for(let i=0;i<posts.length;i++){
      if(posts[i].topicId !== this.props.topic.id){
        diffTopic = true
        break
      }
    }

    if(diffTopic){
      this.props.getPosts({topicLocation: this.props.topicLocation})
    }
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.topic.id !== this.props.topic.id){
      this.props.getPosts({topicLocation: nextProps.topicLocation})
    }
  }

  render(){
    return(
      <div>
        <h1>Recent Posts in {this.props.topic.title}</h1>
        {this.props.topic === {} && (
          <Container style={{ marginBottom: '10em' }}>
            <PageNotFound/>
          </Container>
        )}
        {this.props.posts.length === 0 && (
          <Container style={{ marginBottom: '10em' }}>
            <h3>No posts yet, be the first!</h3>
          </Container>
        )}
        {this.props.posts.length > 0 && (
          <PostCardGrid fill='posts'/>
        )}
        <AuthOnly/>
      </div>
    )
  }
}

function mapStateToProps({user, post, topic}, ownProps){
  const topicLocation = ownProps.match.params.topicLocation
  const catagory = topic.find(t => t.location === topicLocation)
  let t = {}
  if(catagory)
    t = catagory

  return {
    userLoggedIn: user.userLoggedIn,
    posts: post,
    topic: t,
    topicLocation: topicLocation
  }
}

function mapDispachToProps(dispatch){
  return{
    getPosts: (data) => dispatch(getPostsAPI(data)),
    loggedOutUser: (data) => dispatch(loggedOutUser(data))
  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(Topic));
