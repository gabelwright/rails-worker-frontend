import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import { Button, Modal, Menu, Form, TextArea, Grid, Checkbox, Icon, Popup, Container } from 'semantic-ui-react'

import {sendPostAPI} from './../actions/post'

class NewPostModal extends Component{
  state = {
    modalOpen: false,
    title: '',
    body: '',
    error: '',
    anon: false
  }

  getTopicLocation = () => {
    let path = this.props.location.pathname.split('/')
    if(path[1] === 'topic'){
      let t = this.props.topics.find(t => t.location === path[2])
      if(t){
        return t
      }else{
        return {title: null, location: null, about: null}
      }
    }
  }

  sendPost = () => {
    let {title, body, anon} = this.state
    let t = this.getTopicLocation()
    this.props.sendPost({title: title, body, topicId: t.id, topicLocation: t.location, userId: this.props.user.id, anon})
    .then(data => {
      if(!data.error){
        this.setState({modalOpen: false, title: '', body: ''})
        let t = this.getTopicLocation()
        this.props.history.push(`/topic/${t.location}/${data.data.id}`)
      }
      else{
        this.setState({error: data.error})
      }
    })

  }

  changeModal = () => {
    this.setState({modalOpen: !this.state.modalOpen})
  }

  render(){
    return (

      <Modal
        trigger={<Menu.Item onClick={this.changeModal}>New Post</Menu.Item>}
        closeOnRootNodeClick={false}
        closeIcon
        onClose={this.changeModal}
        open={this.state.modalOpen}
      >
        <Modal.Header>New Post to {this.getTopicLocation().title}</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            {this.state.error && (
              <h4>{this.state.error}</h4>
            )}
          <Form onSubmit={this.sendPost}>
            <Form.Field>
              <label>Title</label>
              <input
                onChange={(i) => this.setState({title: i.target.value})}
                value={this.state.title}
              />
            </Form.Field>
            <Form.Field>
              <label>Post</label>
              <TextArea
                autoHeight
                rows='5'
                onChange={(i) => this.setState({body: i.target.value})}
                value={this.state.body}
              />
            </Form.Field>
            <Form.Field>
                <Popup
                  wide
                  trigger={
                    <Container>
                      <Checkbox label='Post Anonymously'
                        onChange={(event, data) => this.setState({anon: data.checked})}
                      />
                      <Icon name='question circle'/>
                    </Container>
                  }
                >
                  Checking this box will prevent your identity from being saved, however, anonymous posts will be removed or censored
                  if they violate BlueBoard's Terms and Conditons.  Once posted, it cannot edited or deleted.
                </Popup>
            </Form.Field>

            <Grid>
              <Grid.Column textAlign='left' width='8'>
                <Button
                  primary
                  type='submit'
                  disabled={!(this.state.title && this.state.body)}
                >
                  Post
                </Button>
              </Grid.Column>
              <Grid.Column textAlign='right' width='8'>
                <Button
                  negative
                  onClick={this.changeModal}
                >
                  Cancel
                </Button>
              </Grid.Column>

            </Grid>

          </Form>
          </Modal.Description>
        </Modal.Content>
      </Modal>
    )
  }
}

function mapStateToProps({user, topic}, ownProps){
  return {
    topics: topic,
    user: user
  }
}

function mapDispachToProps(dispatch){
  return{
    sendPost: (data) => dispatch(sendPostAPI(data)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(NewPostModal));
