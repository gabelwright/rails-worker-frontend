import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'

import {getPostsAPI, getPostsFromAPI} from './../actions/post'
import {loggedOutUser} from './../actions/user'

import PostCardGrid from './PostCardGrid'
import AuthOnly from './AuthOnly'

class Dash extends Component{
  state = {

  }

  componentDidMount(){
    this.props.getPosts({topicLocation: 'recent'})
    .then(data => {
      if(!data.user){
        this.props.history.replace('/login')
      }

    })
  }

  render(){
    return(
      <div>
        <h1>Whats New</h1>
        <PostCardGrid fill='posts'/>
        <AuthOnly/>
      </div>
    )
  }
}

function mapStateToProps({user, post, topic}, ownProps){
  let loading = topic.length === 0
  return {
    userLoggedIn: user.userLoggedIn,
    posts: post,
    loading: loading,
    total_posts: post.length
  }
}

function mapDispachToProps(dispatch){
  return{
    getPosts: (data) => dispatch(getPostsAPI(data)),
    getPostsFrom: (data) => dispatch(getPostsFromAPI(data)),
    loggedOutUser: (data) => dispatch(loggedOutUser(data)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(Dash));
