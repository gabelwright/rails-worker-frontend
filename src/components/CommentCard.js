import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter, Link} from 'react-router-dom'

import {Card} from 'semantic-ui-react'

import {timeSince} from './../utils/helpers'

class CommentCard extends Component{
  state = {

  }

  render(){
    const comment = this.props.comment
    let body = comment.body


    return(
      <Card raised as={Link} to={`/topic/${this.props.topic.location}/${comment.postId}`} style={{ textDecoration: 'none' }}>
        <Card.Content>
          <Card.Header>
            {comment.title}
          </Card.Header>
        </Card.Content>
        <Card.Content description={body} />
        <Card.Content extra>
          {timeSince(comment.createdAt)}
        </Card.Content>
      </Card>
    )
  }
}

function mapStateToProps({topic}, ownProps){

  let t = topic.find(item => item.id === ownProps.comment.topicId)

  return{
    topic: t
  }
}

function mapDispachToProps(dispatch){
  return{

  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(CommentCard));
