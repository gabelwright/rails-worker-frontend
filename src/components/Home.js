import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Container, Grid } from 'semantic-ui-react'

import {whatIsBluBoard} from './../utils/strings'

class Home extends Component{
  state = {}

  render(){
    return(
      <Container textAlign='center'>
        <h1>Welcome to BlueBoard</h1>
        <Grid>
          <Grid.Column width='2'>

          </Grid.Column>
          <Grid.Column width='12'>
            {whatIsBluBoard}
          </Grid.Column>
          <Grid.Column width='2'>

          </Grid.Column>

        </Grid>

        <Container style={{ marginTop: '10em' }}></Container>
      </Container>
    )
  }
}

function mapStateToProps(state, ownProps){
  return {

  }
}

export default connect(mapStateToProps)(Home);
