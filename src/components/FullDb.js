import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import { Icon, Table, Popup, Accordion, Label, Modal, Form, Button, Loader, Container, Input, TextArea } from 'semantic-ui-react'

import * as API from './../utils/api/post'
import * as ADMIN from './../utils/api/admin'
import {timeSince} from './../utils/helpers'

import PageNotFound from './PageNotFound'

class FullDb extends Component{
  state = {
    userTable: [],
    postTable: [],
    showDeleteModal: false,
    deletePostId: 0,
    password: '',
    loading: true,
    allowed: false,
    route: '',
    data: '',
    postResponse: ''
  }

  componentDidMount(){
    ADMIN.getAllUsers().then(data => {
      if(!data.error){
        this.setState({userTable: data.data, allowed: true})
      }
      this.setState({loading: false})
    })

    API.getPosts('recent', 50).then(data => {
      if(!data.error)
        this.setState({postTable: data.data})
      else
        console.log(data.error)
    })
  }

  deletePost = () => {
    ADMIN.deletePost(this.state.deletePostId, this.state.password)
    .then(data => console.log(data))
    let newPostTable = this.state.postTable.filter(p => p.id !== this.state.deletePostId)
    this.setState({showDeleteModal: false, deletePostId: 0, postTable: newPostTable, password: ''})
  }

  sendPOST = () => {
    ADMIN.sendPOST(this.state.route, this.state.data)
    .then(data => {
      this.setState({postResponse: data})
    })
  }

  render(){

    const deleteModal = (
      <Modal
        open={this.state.showDeleteModal}
        size='mini'
        onClose={() => this.setState({showDeleteModal: false})}
        closeIcon={true}
      >
        <Modal.Header>Delete Post</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <Form onSubmit={this.deletePost}>
              <Form.Field>
                <label>Admin Password</label>
                <input
                  type='password'
                  value={this.state.password}
                  onChange={(i) => this.setState({password: i.target.value})}
                />
              </Form.Field>
              <Button negative type='submit'>Delete</Button>
            </Form>
          </Modal.Description>
        </Modal.Content>
      </Modal>
    )

    const tableUsers = (
      <Table celled >
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>id</Table.HeaderCell>
            <Table.HeaderCell>First</Table.HeaderCell>
            <Table.HeaderCell>Last</Table.HeaderCell>
            <Table.HeaderCell>Username</Table.HeaderCell>
            <Table.HeaderCell>Email</Table.HeaderCell>
            <Table.HeaderCell>Confirmed</Table.HeaderCell>
            <Table.HeaderCell>Details</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {this.state.userTable.map(u => {

            return (
              <Table.Row key={u.id}>
                <Table.Cell>{u.id}</Table.Cell>
                <Table.Cell>{u.first}</Table.Cell>
                <Table.Cell>{u.last}</Table.Cell>
                <Table.Cell>{u.username}</Table.Cell>
                <Table.Cell>{u.email}</Table.Cell>
                <Table.Cell textAlign='center'>
                  {u.confirmed && (
                    <Icon color='green' name='checkmark' size='large' />
                  )}
                </Table.Cell>
                <Table.Cell textAlign='center'>
                  <Popup
                    trigger={<Icon name='question circle' size='large'/>}
                    wide='very'
                  >
                    <Popup.Content>
                      <div><strong>Created At:</strong> {u.createdAt}</div>
                      <div><strong>Updated At:</strong> {u.updatedAt}</div>
                    </Popup.Content>
                  </Popup>
                </Table.Cell>
              </Table.Row>
            )
          })}
        </Table.Body>
      </Table>
    )

    const tablePosts = (
      <Table celled >
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>id</Table.HeaderCell>
            <Table.HeaderCell>Title</Table.HeaderCell>
            <Table.HeaderCell>Body</Table.HeaderCell>
            <Table.HeaderCell>User</Table.HeaderCell>
            <Table.HeaderCell>Topic</Table.HeaderCell>
            <Table.HeaderCell>Comments/ Views</Table.HeaderCell>
            <Table.HeaderCell>Poster</Table.HeaderCell>
            <Table.HeaderCell>Posted</Table.HeaderCell>
            <Table.HeaderCell>Delete</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {this.state.postTable.map(u => {

            return (
              <Table.Row key={u.id}>
                <Table.Cell>{u.id}</Table.Cell>
                <Table.Cell>{u.title}</Table.Cell>
                <Table.Cell textAlign='center'>
                  <Popup
                    trigger={<Icon name='question circle' size='large'/>}
                    wide='very'
                  >
                    <Popup.Content>
                      {u.body}
                    </Popup.Content>
                  </Popup>
                </Table.Cell>
                <Table.Cell>{u.userId}</Table.Cell>
                <Table.Cell>{u.topicId}</Table.Cell>
                <Table.Cell>{u.comments}/{u.views}</Table.Cell>
                <Table.Cell>{u.poster}</Table.Cell>
                <Table.Cell>{timeSince(u.createdAt)}</Table.Cell>
                <Table.Cell textAlign='center'>
                  <Icon
                    name='delete'
                    color='red'
                    size='large'
                    onClick={() => this.setState({showDeleteModal: true, deletePostId: u.id})}
                  />
                </Table.Cell>
              </Table.Row>
            )
          })}
        </Table.Body>
      </Table>
    )

    const tableTopics = (
      <Table celled >
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>id</Table.HeaderCell>
            <Table.HeaderCell>Title</Table.HeaderCell>
            <Table.HeaderCell>About</Table.HeaderCell>
            <Table.HeaderCell>Location</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {this.props.topicTable.map(u => {

            return (
              <Table.Row key={u.id}>
                <Table.Cell>{u.id}</Table.Cell>
                <Table.Cell>{u.title}</Table.Cell>
                <Table.Cell>{u.about}</Table.Cell>
                <Table.Cell>{u.location}</Table.Cell>
              </Table.Row>
            )
          })}
        </Table.Body>
      </Table>
    )
    
    const postrequest = (
      <Form onSubmit={this.sendPOST}>
        <Form.Field>
          <label>Route</label>
          <Input 
            onChange={(i) => this.setState({route: i.target.value})}
            value={this.state.route}
          />
        </Form.Field>
        <Form.Field>
          <label>Data</label>
          <TextArea 
            onChange={(i) => this.setState({data: i.target.value})}
            value={this.state.data}
          >
          </TextArea>
        </Form.Field>
        <Button type='submit' primary>Send</Button>
        <div style={{paddingTop: '15px'}}>
          {this.state.postResponse}
        </div>
      </Form>
    )

    const panels = [
      {title: {
        content: <Label color='black' content='Users' />,
        key: 4,
      },
        content: {
          content: tableUsers,
          key: 1
        }
      },
      {title: {
        content: <Label color='black' content='Posts' />,
        key: 5,
      },
        content: {
          content: tablePosts,
          key: 2
        }
      },
      {title: {
        content: <Label color='black' content='Topics' />,
        key: 6,
      },
        content: {
          content: tableTopics,
          key: 3
        }
      },
      {title: {
        content: <Label color='black' content='POST Request' />,
        key: 7,
      },
        content: {
          content: postrequest,
          key: 8
        }
      },
    ]

    return(
      <div>
        {this.state.loading && (
          <Loader active/>
        )}
        {!this.state.loading && (
          <Container>
            {this.state.allowed && (
              <Container>
                {deleteModal}
                <Accordion panels={panels} exclusive={false} fluid />
              </Container>
            )}

          {!this.state.allowed && (
            <PageNotFound/>
          )}
          </Container>

        )}



      </div>
    )
  }
}

function mapStateToProps(state, ownProps){
  return {
    topicTable: state.topic
  }
}

function mapDispachToProps(dispatch){
  return{

  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(FullDb));
