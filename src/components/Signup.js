import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import {Form, Message, Input, Modal, Header, Button, Icon} from 'semantic-ui-react'

import {newUserAPI} from './../actions/user'
import {termsAndContions} from './../utils/strings'

import Avatar from './Avatar'

class Signup extends Component{
  state = {
    first: '',
    last: '',
    username: '',
    password: '',
    confirm: '',
    error: '',
    showTerms: false
  }

  componentDidMount(){

  }

  allowSignup = () => {
    const {username, password, confirm} = this.state
    if(!username){
      this.setState({
        error: 'Please provide your EPISD username.  This will be used to verify that you are a teacher.',
        password: '',
        confirm: ''
      })
      return false
    }
    else if(password.length < 6 || !/\d/.test(password)){
      this.setState({
        error: 'Password does not satisfy the password requirements.  It must be at least 6 characters long and contain a number.',
        password: '',
        confirm: ''
      })
      return false
    }
    else if(password !== confirm){
      this.setState({
        error: 'Password and Confirm Password fields do not match.',
        password: '',
        confirm: ''
      })
      return false
    }
    else
      return true
  }

  changeModal = () => {
    this.setState({showTerms: !this.state.showTerms})
  }

  openTermsModal = () => {
    if(this.allowSignup()){
      this.changeModal()
    }
  }

  sendSignup = () => {
    if(this.allowSignup()){
      this.props.newUser({
        first: this.state.first,
        last: this.state.last,
        username: this.state.username,
        email: `${this.state.username}@episd.org`,
        password: this.state.password
      }).then(data => {
        if(data.error){
          this.setState({error: data.error, password: '', confirm: ''})
          this.changeModal()
        }
        else{
          this.setState({
            first: '',
            last: '',
            username: '',
            password: '',
            confirm: ''
          })
          this.props.history.push('/login')
        }

      })

    }
  }

  render() {
    const {first, last, username, password, confirm} = this.state

    const formInstance = (
      <Form onSubmit={this.openTermsModal}>
        {this.state.error && (
          <Message negative>
            <Message.Header>Error</Message.Header>
            <p>{this.state.error}</p>
          </Message>
        )}

        <Form.Group>
          <Form.Input
            width='7'
            required
            value={first}
            onChange={(i) => this.setState({first: i.target.value.trim()})}
            label='First name'
            placeholder='First name'
          />
          <Form.Input
            width='7'
            required
            value={last}
            onChange={(i) => this.setState({last: i.target.value.trim()})}
            label='Last name'
            placeholder='Last name'
            icon={{name: '', size: 'large', color: 'red'}}
          />
          {(this.state.first || this.state.last) && (
            <Form.Field>
              <Avatar name={this.state.first + ' ' + this.state.last} size={90}/>
            </Form.Field>
          )}

        </Form.Group>

        <Form.Group >
          <Form.Field required>
            <label>Username/Email</label>
            <Input
              label={{ basic: true, content: '@episd.org' }}
              labelPosition='right'
              value={username}
              onChange={(i) => this.setState({username: i.target.value.trim()})}
            />
          </Form.Field>
        </Form.Group>

        <Form.Group widths='equal'>
          <Form.Input
            fluid
            required
            value={password}
            onChange={(i) => this.setState({password: i.target.value})}
            type='password'
            label='Password'
          />
          <Form.Input
            fluid
            required
            value={confirm}
            onChange={(i) => this.setState({confirm: i.target.value})}
            type='password'
            label='Confirm Password'
          />

        </Form.Group>
        <Message
          content='Passwords must be at least 6 characters long and contain a letter and a number.'
        />

        <Form.Button primary >Submit</Form.Button>
      </Form>
    );

    const termsModal = (
      <Modal
        open={this.state.showTerms}
        dimmer='blurring'
        closeOnRootNodeClick={false}
        onClose={this.changeModal}
        size='small'
      >
        <Header as='h1' content='Terms and Conditions of Use' />
        <Modal.Content>
          {termsAndContions}
        </Modal.Content>
        <Modal.Actions>
          <Button color='red' inverted onClick={() => this.setState({showTerms: false, error: 'You must accept the Terms and Conditions to continue.'})}>
            <Icon
              name='remove'
            /> Disagree
          </Button>
          <Button color='green' inverted onClick={this.sendSignup}>
            <Icon
              name='checkmark'
            /> Agree
          </Button>
        </Modal.Actions>
      </Modal>
    )

    return (
      <div>
        {termsModal}
        {formInstance}
      </div>
    )
  }
}

function mapStateToProps(state, ownProps){
  return {

  }
}

function mapDispachToProps(dispatch){
  return{
    newUser: (data) => dispatch(newUserAPI(data))
  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(Signup));
