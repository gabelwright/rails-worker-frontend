import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'

import {Container, Menu, Grid, Label} from 'semantic-ui-react'
import {getProfileAPI, verifyTokenAPI} from './../actions/user'

import PostCardGrid from './PostCardGrid'
import ChangePasswordModal from './ChangePasswordModal'
import VerifyEmailModal from './VerifyEmailModal'

class Profile extends Component{
  state = {
    activeItem: this.props.location.search ? this.props.location.search.replace('?view=', '') : 'settings',
    oldPassword: '',
    newPassword: '',
    token: ''
  }

  componentDidMount(){

    if(this.props.match.params.token){
      this.props.verifyToken({token: this.props.match.params.token})
      .then(d => {
        if(!d.error)
          this.props.history.replace('/profile')
      })
    }

    this.props.getProfile()
    .then(d => {
      if(!d.user)
        this.props.history.replace('/login')
      else if(!d.user.confirmed){

      }
    })
  }

  changeActiveItem = (e, { name }) => this.setState({ activeItem: name })

  render(){

    const activeItem = this.state.activeItem

    return(
      <Container>
        <Menu pointing>
          <Menu.Item name='settings' active={activeItem === 'settings'} onClick={this.changeActiveItem}>My Info</Menu.Item>
          <Menu.Item name='posts' active={activeItem === 'posts'} onClick={this.changeActiveItem}>My Posts</Menu.Item>
          <Menu.Item name='comments' active={activeItem === 'comments'} onClick={this.changeActiveItem}>My Comments</Menu.Item>
        </Menu>

        {this.state.activeItem !== 'settings' && (
          <PostCardGrid fill={this.state.activeItem}/>
        )}
        {this.state.activeItem === 'settings' && (
          <Grid>
            {this.props.user.username && !this.props.user.confirmed && (
              <Grid.Row >
                <Grid.Column>
                  <VerifyEmailModal/>
                </Grid.Column>
              </Grid.Row>
            )}
            <Grid.Row>
              <Grid.Column>
                <h3>Personal Info</h3>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width='2' textAlign='right'><strong>Name:</strong></Grid.Column>
              <Grid.Column width='14'>{this.props.user.first} {this.props.user.last}</Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width='2' textAlign='right'><strong>Email:</strong></Grid.Column>
              <Grid.Column width='14'>
                {this.props.user.email}
                {this.props.user.confirmed && (
                  <Label horizontal color='green' style={{marginLeft: '8px'}}>
                    Verified
                  </Label>

                )}
                {!this.props.user.confirmed && (
                  <Label horizontal color='red' style={{marginLeft: '8px'}}>
                    Unverified
                  </Label>

                )}

              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width='2' textAlign='right'><strong>Password:</strong></Grid.Column>
              <Grid.Column width='14'>
                <ChangePasswordModal/>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        )}
      </Container>
    )
  }
}

function mapStateToProps({post, topic, user}, ownProps){

  return {
    posts: post,
    user: user,
  }
}

function mapDispachToProps(dispatch){
  return{
    getProfile: (data) => dispatch(getProfileAPI(data)),
    verifyToken: (data) => dispatch(verifyTokenAPI(data)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(Profile));
