import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter, Link} from 'react-router-dom'

import {Card, Icon, Grid} from 'semantic-ui-react'

import {timeSince} from './../utils/helpers'

class PostCard extends Component{
  state = {

  }

  render(){
    const post = this.props.post
    let title = post.title.length > 25 ? post.title.substring(0, 25) + '...' : post.title
    let body = post.body.length > 75 ? post.body.substring(0, 75) + '...' : post.body
    let topic = this.props.topics ? this.props.topics.find(t => t.id === post.topicId) : {location: ''}
    let cornor = topic ? topic.title : ''

    return(
      <Card raised as={Link} to={`/topic/${topic.location}/${post.id}`} style={{ textDecoration: 'none' }}>
        <Card.Content>
          <Card.Header>
            {title}
          </Card.Header>
          <Grid>
            <Grid.Column floated='left' width={8}>
              <Card.Meta>{post.poster}</Card.Meta>
            </Grid.Column>
            <Grid.Column textAlign='right' width={8}>
              <Card.Meta textAlign='right'>{timeSince(post.createdAt)}</Card.Meta>
            </Grid.Column>
          </Grid>


        </Card.Content>
        <Card.Content description={body} />
        <Card.Content extra>
          <Grid>
            <Grid.Column floated='left' width={12}>
              {cornor}
            </Grid.Column>
            <Grid.Column textAlign='right' width={4}>
              {post.comments} <Icon name='comments outline'/>
            </Grid.Column>
          </Grid>
        </Card.Content>
      </Card>
    )
  }
}

function mapStateToProps({user, post, topic}, ownProps){
  return{
    topics: topic
  }
}

function mapDispachToProps(dispatch){
  return{

  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(PostCard));
