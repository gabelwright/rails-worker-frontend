import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'

import {Modal, Button, Message, Grid} from 'semantic-ui-react'

import {sendVerify} from './../utils/api/user'
import {addSpaces} from './../utils/helpers'

class VerifyEmailModal extends Component{
  state = {
    firstModal: false,
    secondModal: false,
  }

  resendEmail = () => {
    let {first, last, email} = this.props.user
    sendVerify(first, last, email)
    .then(data => console.log(data))
  }

  render(){

    // const nextModal = (
    //   <Modal trigger={
    //       <Button color='green' size='tiny' compact onClick={this.resendEmail}>
    //         Resend Email
    //       </Button>
    //     }
    //     size='mini'
    //     closeIcon
    //   >
    //     <Modal.Header>Email Sent</Modal.Header>
    //       <Modal.Content>
    //       <Modal.Description>
    //         Another email has been sent to {this.props.user.email}. Please check your
    //         spam folder.
    //       </Modal.Description>
    //       </Modal.Content>
    //   </Modal>
    // )

    const trigger = (
      <Button fluid style={{padding: '0px'}}>
        <Message negative>
          <Message.Content>
            Please click here to verify your email address.
          </Message.Content>
        </Message>
      </Button>
    )

    const verifyModal = (
      <Modal trigger={trigger} closeIcon size='small'>
        <Modal.Header>Verify Email</Modal.Header>
        <Modal.Content>

          <Modal.Description>
            <Grid>
            <Grid.Column width='16'>


            <p><strong>Send BlueBoard an email with the following info:</strong></p>
              <ul>
              <li><strong>To:</strong> verify@theblueboard.org</li>
              <li><strong>Subject:</strong> 'Verify My Email'</li>
              <li><strong>Body:</strong></li>
              <p>{'(start)'+this.props.token+'(end)'}</p>
              <p><strong>Do not include any attachements or signatures</strong></p>
              </ul>
              After it is sent, refresh the page.
            </Grid.Column>
            </Grid>
          </Modal.Description>
        </Modal.Content>
      </Modal>
    )

    return(
      <span>{verifyModal}</span>
    )
  }
}

function mapStateToProps({post, topic, user}, ownProps){
  return {
    posts: post,
    user: user,
    token: addSpaces(user.token)
  }
}

function mapDispachToProps(dispatch){
  return{

  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(VerifyEmailModal));
