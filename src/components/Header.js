import React, { Component } from 'react';
import {connect} from 'react-redux'
import {Route, withRouter, Switch, Link} from 'react-router-dom'
import { Container, Menu, Dropdown, Sidebar, Icon, Grid } from 'semantic-ui-react'

import Dash from './Dash'
import PageNotFound from './PageNotFound';
import Login from './Login';
import Signup from './Signup';
import FullDb from './FullDb'
import Home from './Home'
import Topic from './Topic'
import PostPage from './PostPage'
import NewPostModal from './NewPostModal'
import Profile from './Profile'

import {loggedOutUserAPI, checkForLoggedIn} from './../actions/user'
import {loggedOutPost, getPostsAPI} from './../actions/post'
import {getTopicsAPI} from './../actions/topic'
import Avatar from './Avatar'

class Header extends Component {
  state = {
    visible: false,
    userLoggedIn: false,
    first: '',
    topics: []
  }

  componentDidMount(){
    this.props.getTopics()
    this.props.checkForLoggedIn()
  }

  toggleVisibility = (e, { name }) => this.setState({ visible: !this.state.visible })

  logoutUser = () => {
    this.props.loggedOutUser()
    this.props.loggedOutPost()
    this.props.history.push('/login')
  }

  render() {
    const routes = (
      <Switch>
        <Route exact path='/' component={Home}/>
        <Route exact path='/login' component={Login}/>
        <Route exact path='/signup' component={Signup}/>
        <Route exact path='/dash' component={Dash}/>
        <Route exact path='/verify/:token' component={Profile}/>
        <Route exact path='/profile' component={Profile}/>
        <Route exact path='/topic/:topicLocation' component={Topic}/>
        <Route exact path='/topic/:topicLocation/:postid' component={PostPage}/>
        <Route exact path='/fulldb' component={FullDb}/>
        <Route path='/' component={PageNotFound}/>
      </Switch>
    )

    const topics = (
      this.props.topics.map(t => {
        return(
          <Menu.Item key={t.location}
            as='a'
            onClick={() => {
              this.setState({visible: false})
              this.props.history.push(`/topic/${t.location}`)}
            }
            disabled={!this.props.userLoggedIn}
          >
            {t.title}
          </Menu.Item>
        )
      })
    )

    const sidebar = (
      <Sidebar as={Menu} animation='push' width='thin' visible={this.state.visible} vertical inverted>
        <Menu.Item >
          Topics:
        </Menu.Item>

        {topics}
      </Sidebar>
    )

    const top_menu = (
      <Menu fixed='top' inverted>
        <Container>
          <Menu.Item
            as='a'
            onClick={this.toggleVisibility}
          >
            <Icon name='sidebar' />
          </Menu.Item>

          {this.props.userLoggedIn && (
            <Menu.Item as={Link} to='/dash'>Dashboard</Menu.Item>
          )}

          {this.props.userLoggedIn && this.props.location.pathname.includes('/topic/') && this.props.topics && (
            <NewPostModal/>
          )}

          {this.props.userLoggedIn && (
            <Menu.Menu position='right'>
              <Avatar size={45} background={[27,28,29,221]}/>
              <Dropdown item simple text={'Logged in as ' + this.props.first} position='right'>

                <Dropdown.Menu>
                  <Dropdown.Item as={Link} to='/profile'>Profile</Dropdown.Item>
                  <Dropdown.Item onClick={this.logoutUser}>
                    Logout
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Menu.Menu>
          )}

          {!this.props.userLoggedIn && (
            <Menu.Menu position='right'>
              <Menu.Item as={Link} to='/login'>Login</Menu.Item>
              <Menu.Item as={Link} to='/signup'>Signup</Menu.Item>
            </Menu.Menu>
          )}
        </Container>
      </Menu>
    )

    const sidebar_pusher = (
      <Sidebar.Pusher>
        {top_menu}
        <Container style={{ marginTop: '5em', padding: '15px' }}>
          {routes}
        </Container>
        <Container style={{ marginTop: '10em' }}></Container>
      </Sidebar.Pusher>
    )

    return (
      <div>
        <Grid columns={1}>
          <Grid.Row stretched>
            <Container style={{ marginTop: '1em' }}>
              <Sidebar.Pushable>
                {sidebar}
                {sidebar_pusher}
              </Sidebar.Pushable>
            </Container>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

function mapStateToProps({user, topic}, ownProps){
  return {
    first: user.first,
    userLoggedIn: user.userLoggedIn,
    username: user.username,
    topics: topic
  }
}

function mapDispachToProps(dispatch){
  return{
    loggedOutUser: () => dispatch(loggedOutUserAPI()),
    loggedOutPost: () => dispatch(loggedOutPost()),
    checkForLoggedIn: () => dispatch(checkForLoggedIn()),
    getTopics: () => dispatch(getTopicsAPI()),
    getPosts: () => dispatch(getPostsAPI()),
  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(Header));
