import {combineReducers} from 'redux'
import user from './user'
import post from './post'
import topic from './topic'
import comment from './comment'

export default combineReducers({
  user, post, topic, comment
})
