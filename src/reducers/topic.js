import {GET_TOPICS} from './../actions/topic'

function topic(state = [], action){

  if(action.type === GET_TOPICS){
    const {data} = action
    return data
  }

  else
    return state

}

export default topic;
