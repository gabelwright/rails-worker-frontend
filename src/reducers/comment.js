import {GET_COMMENTS, ADD_COMMENT, LOGGED_OUT, DELETE_COMMENT} from './../actions/comment'

function comment(state = [], action){

  if(action.type === GET_COMMENTS){
    let {data} = action
    return data
  }

  else if(action.type === ADD_COMMENT){
    let newState = []
    for(let i=0;i<state.length;i++){
      newState.push(state[i])
    }
    newState.push(action.comment)
    return newState

  }

  else if(action.type === DELETE_COMMENT){
    let id = action.id
    let newState = []
    for(let i=0;i<state.length;i++){
      if(state[i].id === id){
        state[i].body = '[deleted]'
        state[i].deleted = true
      }
      newState.push(state[i])
    }
    return newState
  }

  else if(action.type === LOGGED_OUT){
    return []
  }

  else
    return state

}

export default comment;
