import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducer from './../reducers'

let store

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

  store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)))

} else {
  store = createStore(reducer, applyMiddleware(thunk))
}

export default store;
