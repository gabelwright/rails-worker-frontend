import {website, localhost} from './../strings'

let URL = ''

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    URL = `${localhost}/comment/`
} else {
    URL = `${website}/comment/`
}

const headers = {
  'Content-Type': 'application/json',
}

export const getComments = (postid) => {
  const u = `${URL}get`
  return fetch(u, {
    method: 'POST',
    headers: headers,
    credentials: 'include',
    body: JSON.stringify({postid})
  }).then(res => res.text() )
    .then(data => JSON.parse(data))
}

export const addComment = (body, poster, userId, postid, parentId, topicId) => {
  const u = `${URL}add`
  return fetch(u, {
    method: 'POST',
    headers: headers,
    credentials: 'include',
    body: JSON.stringify({body, poster, userId, postid, parentId, topicId})
  }).then(res => res.text() )
    .then(data => JSON.parse(data))
}

export const deleteComment = (id) => {
  const u = `${URL}delete`
  return fetch(u, {
    method: 'DELETE',
    headers: headers,
    credentials: 'include',
    body: JSON.stringify({id})
  }).then(res => res.text() )
    .then(data => JSON.parse(data))
}
