import {website, localhost} from './../strings'

let URL = ''

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    URL = `${localhost}/user/`
} else {
    URL = `${website}/user/`
}

const headers = {
  'Content-Type': 'application/json',
 }

export const newUser = (first, last, username, email, password) => {
 const u = `${URL}add`
 return fetch(u, {
   method: 'POST',
   headers: headers,
   body: JSON.stringify({first, last, username, email, password})
 }).then( res => res.text() )
   .then((data) => JSON.parse(data))
}

export const getLoggedInUser = () => {
 const u = `${URL}get`
 return fetch(u, {
   method: 'GET',
   headers: headers,
   credentials: 'include',
 }).then( res => res.text() )
   .then((data) => JSON.parse(data))
}

export const login = (username, password, remember) => {
  const u = `${URL}login`
  return fetch(u, {
    method: 'POST',
    headers: headers,
    credentials: 'include',
    body: JSON.stringify({username, password, remember})
  }).then( res => res.text() )
    .then((data) => JSON.parse(data))
}

export const updatePassword = (oldPassword, newPassword) => {
  const u = `${URL}update_password`
  return fetch(u, {
    method: 'POST',
    headers: headers,
    credentials: 'include',
    body: JSON.stringify({oldPassword, newPassword})
  }).then( res => res.text() )
    .then((data) => JSON.parse(data))
}

export const getProfile = () => {
  const u = `${URL}profile`
  return fetch(u, {
    method: 'GET',
    headers: headers,
    credentials: 'include',
  }).then(res => res.text())
    .then(data => JSON.parse(data))
}

export const logout = () => {
  const u = `${URL}logout`
  return fetch(u, {
    method: 'POST',
    headers: headers,
    credentials: 'include'
  }).then( res => res.text() )
    .then((data) => JSON.parse(data))
}

export const getToken = (email) => {
  const u = `${URL}get_token`
  return fetch(u, {
    method: 'POST',
    headers: headers,
    credentials: 'include',
    body: JSON.stringify({email})
  }).then( res => res.text() )
    .then((data) => JSON.parse(data))
}

export const sendVerify = (first, last, email) => {
  const u = `${URL}send_verify`
  return fetch(u, {
    method: 'POST',
    headers: headers,
    credentials: 'include',
    body: JSON.stringify({first, last, email})
  }).then( res => res.text() )
    .then((data) => JSON.parse(data))
}

export const verifyToken = (token) => {
  const u = `${URL}verify_token`
  return fetch(u, {
    method: 'POST',
    headers: headers,
    credentials: 'include',
    body: JSON.stringify({token})
  }).then( res => res.text() )
    .then((data) => JSON.parse(data))
}
