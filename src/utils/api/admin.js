import {website, localhost} from './../strings'

let URL = ''

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    URL = `${localhost}/admin/`
} else {
    URL = `${website}/admin/`
}

const headers = {
  'Content-Type': 'application/json',
 }

export const getAllUsers = () => {
  const u = `${URL}get_users`
  return fetch(u, {
    method: 'GET',
    headers: headers,
    credentials: 'include',
  }).then( res => res.text() )
    .then((data) => JSON.parse(data))
}

export const checkAuth = () => {
  const u = `${URL}test`
  return fetch(u, {
    method: 'POST',
    headers: headers,
    credentials: 'include',
  }).then( res => res.text() )
    .then((data) => JSON.parse(data))
}

export const deletePost = (id, password) => {
  const u = `${URL}delete_post`
  return fetch(u, {
    method: 'DELETE',
    headers: headers,
    credentials: 'include',
    body: JSON.stringify({id, password})
  }).then(res => res.text() )
    .then(data => JSON.parse(data))
}

export const sendPOST = (route, data) => {
  const u = `${URL}${route}`
  return fetch(u, {
    method: 'POST',
    headers: headers,
    credentials: 'include',
    body: data
  }).then( res => res.text() )
}
