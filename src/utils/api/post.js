import {website, localhost} from './../strings'

let URL = ''

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    URL = `${localhost}/post/`
} else {
    URL = `${website}/post/`
}

const headers = {
  'Content-Type': 'application/json',
}

export const getTopics = () => {
  const u = `${URL}get_topics`
  return fetch(u, {
    method: 'GET',
    headers: headers,
    // credentials: 'include',
  }).then( res => res.text() )
    .then((data) => JSON.parse(data))
}

export const getPosts = (topicLocation, limit=27) => {
  const u = `${URL}get`
  return fetch(u, {
    method: 'POST',
    headers: headers,
    credentials: 'include',
    body: JSON.stringify({topicLocation, limit})
  }).then( res => res.text() )
    .then((data) => JSON.parse(data))
}

export const getPostsFrom = (timestamp, topicLocation, limit=18) => {
  const u = `${URL}get_from`
  return fetch(u, {
    method: 'POST',
    headers: headers,
    credentials: 'include',
    body: JSON.stringify({timestamp, topicLocation, limit})
  }).then( res => res.text() )
    .then((data) => JSON.parse(data))
}

export const getSinglePost = (postid, location) => {
  const u = `${URL}get_single`
  return fetch(u, {
    method: 'POST',
    headers: headers,
    credentials: 'include',
    body: JSON.stringify({postid, location})
  }).then( res => res.text() )
    .then((data) => JSON.parse(data))
}


export const deletePost = (id) => {
  const u = `${URL}delete`
  console.log('api', id)
  return fetch(u, {
    method: 'DELETE',
    headers: headers,
    credentials: 'include',
    body: JSON.stringify({id})
  }).then(res => res.text() )
    .then(data => JSON.parse(data))
}

export const sendPost = (title, body, topicId, userId, anon) => {
  const u = `${URL}add`
  return fetch(u, {
    method: 'POST',
    headers: headers,
    credentials: 'include',
    body: JSON.stringify({title, body, topicId, userId, anon})
  }).then(res => res.text() )
    .then(data => JSON.parse(data))
}

export const editPost = (postid, body) => {
  const u = `${URL}edit`
  return fetch(u, {
    method: 'POST',
    headers: headers,
    credentials: 'include',
    body: JSON.stringify({postid, body})
  }).then(res => res.text() )
    .then(data => JSON.parse(data))

}
